from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token

class AccountIn(BaseModel):
    username: str
    fact: str
    email: str
    password: str

class AccountWithHashedPassword(BaseModel):
    username: str
    fact: str
    email: str
    hashed_password: str


class AccountFull(BaseModel):
    username: str
    fact: str
    email: str
    hashed_password: str
    account_id: int

class AccountOut(BaseModel):
    username: str
    fact: str
    email: str
    account_id: int

class AccountToken(Token):
    account: AccountOut
