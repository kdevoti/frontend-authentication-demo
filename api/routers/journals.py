from fastapi import APIRouter, Depends, HTTPException
from models.journals import JournalIn, JournalOut, JournalListOut
from models.accounts import AccountFull
from queries.journals import JournalRepo
from authenticator import authenticator

router = APIRouter()


@router.post("/api/journals/")
async def create_journal(
    journal: JournalIn,
    journal_repo: JournalRepo = Depends(),
) -> JournalOut:
    return journal_repo.create(journal)


@router.get("/api/journals/{journal_id}")
async def get_journal(
    journal_id: int,
    journal_repo: JournalRepo = Depends(),
    account_data: dict | None = Depends(authenticator.get_current_account_data)
) -> JournalOut:
    result = journal_repo.get(journal_id)
    if result is None:
        raise HTTPException(status_code=404, detail="No journal found with id {}".format(journal_id))
    elif result.account_id != account_data["account_id"]: 
        raise HTTPException(status_code=403, detail="Unauthorized - current account id {} does not own resource".format(account_data["account_id"]))
    else:
        return result

@router.get("/api/journals")
async def get_all_journals(
    journal_repo: JournalRepo = Depends(),
    account_data: dict | None = Depends(authenticator.get_current_account_data)
) -> JournalListOut:
    result = journal_repo.get_all(account_data["account_id"])
    if result is None:
        return {"journals": []}
    else:
        return {"journals": result}

