from models.journals import JournalIn, JournalOut
from fastapi import HTTPException
from psycopg_pool import ConnectionPool
import os

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])

class JournalRepo:
    def create(
            self, 
            journal: JournalIn
    ) -> JournalOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute(
                        """
                        INSERT INTO journal
                        (account_id, date, content)
                        VALUES (%s, %s, %s)
                        RETURNING ID, ACCOUNT_ID, DATE, CONTENT
                        """,
                        [journal.account_id, journal.date, journal.content],
                    )
                    ac = cur.fetchone()
                    if ac is None:
                        return "blah"
                    else:
                        print("date?", type(ac[2]))
                        return JournalOut(
                            id=ac[0],
                            account_id=ac[1],
                            date=ac[2],
                            content=ac[3],
                        )
                except Exception as e: 
                    raise e


    def get(
            self,
            id: int
    ) -> JournalOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, account_id, date, content
                    FROM journal
                    WHERE id = %s;
                    """,
                    [id],
                )
                ac = cur.fetchone()
                if ac is None:
                    return None 
                else:
                    try:
                        return JournalOut(
                            id=ac[0],
                            account_id=ac[1],
                            date=ac[2],
                            content=ac[3],
                        )
                    except Exception as e:
                        raise Exception("Error:", e)
                    
    def get_all(
            self,
            account_id: int
    ) -> JournalOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT id, account_id, date, content
                    FROM journal
                    WHERE account_id = %s;
                    """,
                    [account_id],
                )
                rows = cur.fetchall()
                if rows is None:
                    return None 
                else:
                    journals = []
                    for row in rows:
                        journal = JournalOut(
                            id=row[0],
                            account_id=row[1],
                            date=row[2],
                            content=row[3]
                        )
                        journals.append(journal)
                    return journals