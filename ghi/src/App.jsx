import { Main } from "./Main";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import SignupForm from "./SignupForm";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import TitleBar from "./TitleBar";
import Profile from "./Profile";

import "./App.css";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  return (
    <BrowserRouter basename={basename}>
      <AuthProvider baseUrl={process.env.REACT_APP_API_HOST}>
        <TitleBar />
        <Routes>
          <Route exact path="/" element={<Main />}></Route>
          <Route exact path="/signup" element={<SignupForm />}></Route>
          <Route path="/account/:accountId" element={<Profile />}></Route>
        </Routes>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
